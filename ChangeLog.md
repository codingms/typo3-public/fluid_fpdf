# Fluid-FPDF for TYPO3 Change-Log

## 2025-01-21 Release of version 2.7.4

*	[BUGFIX] Insert missing PluginUtility for plugin registration



## 2025-01-19 Release of version 2.7.3

*	[TASK] Fix code style



## 2025-01-17 Release of version 2.7.2

*	[BUGFIX] Remove migration command from services.yaml



## 2025-01-17 Release of version 2.7.1

*	[TASK] Add new content element wizard and missing icon registration
*	[TASK] Migrate registering page-tsconfig
*	[TASK] Migrate to plugins to content elements and icon registration
*	[TASK] Remove signal/slot from older TYPO3 versions



## 2025-01-xx Release of version 2.7.0

*	[TASK] Migrate to TYPO3 13, remove support for TYPO3 11
*	[TASK] Migrate utf8_decode functionality
*	[TASK] Migrate utf8_encode functionality
*	[TASK] Migrate TypoScript imports
*	[TASK] Update feature listing in readme files



## 2023-12-10 Release of version 2.6.0

*	[FEATURE] Add view helper for rotating text



## 2023-12-05 Release of version 2.5.2

*	[BUGFIX] WriteHtmlTrait::PutLink() calls Write() with fix line height
*	[TASK] Optimize version conditions in PHP code



## 2023-11-13 Release of version 2.5.1

*	[BUGFIX] Fix bullet list and paragraph line breaks in HTML printing



## 2023-11-10 Release of version 2.5.0

*	[FEATURE] Introduce dl, dt, dd tags in HTML for definition lists for HtmlMultiCell
*	[BUGFIX] Improve HTML tag documentation in features



## 2023-11-02  Release of version 2.4.6

*	[BUGFIX] Remove red debugging table border



## 2023-11-01  Release of version 2.4.5

*	[TASK] Clean up documentation



## 2023-10-27  Release of version 2.4.4

*	[BUGFIX] Fix access on non defined ctype index in backend preview



## 2023-09-22  Release of version 2.4.3

*	[TASK] Refactor and optimize HtmlMultiCellViewHelper
*	[TASK] Introduce <pagebreak> tag in HTML for triggering a PDF page break
*	[TASK] Update and extend documentation



## 2023-09-20  Release of version 2.4.2

*	[TASK] Introduce listMarginBottom listIndentMargin attribute for HtmlMultiCellViewHelper



## 2023-09-13  Release of version 2.4.1

*	[BUGFIX] Fix SetFontViewHelper for nullable style argument



## 2023-09-04  Release of version 2.4.0

*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10



## 2023-02-23  Release of version 2.3.7

*	[TASK] Generated documentation for TypoScript constants
*	[TASK] Normalize and migrate configuration files
*	[BUGFIX] Fix undefined array index in CellViewHelper for PHP 8+
*	[BUGFIX] Fix H tags and gaps after UL tags



## 2022-11-14  Release of version 2.3.6

*	[BUGFIX] Suppress warning in WriteHtmlTrait
*	[BUGFIX] Fix type issue in Barcode trait



## 2022-07-28  Release of version 2.3.5

*	[BUGFIX] Suppress warning on file_exists in Image ViewHelper



## 2022-05-27  Release of version 2.3.4

*	[BUGFIX] Fix image paths for TYPO3 11
*	[BUGFIX] Fix view helper namespaces



## 2022-05-12  Release of version 2.3.3

*	[TASK] Add replace and explode ViewHelper



## 2022-04-23  Release of version 2.3.2

*	[TASK] Insert index space check
*	[TASK] Move extension icon into public folder
*	[BUGFIX] Fix casting issue in index creation



## 2022-04-16  Release of version 2.3.1

*	[TASK] Add condition for ADD_PAGE in HTML output for triggering new pages
*	[BUGFIX] Fix spaces in beginning of a line after a line break



## 2022-02-07  Release of version 2.3.0

*	[TASK] Optimize code style
*	[TASK] Clean up Documentation files
*	[TASK] Clean up PHP sources and dividing by traits
*	[BUGFIX] Fix documentation configuration
*	[TASK] Update FPDF library to version 1.84
*	[TASK] Migration for TYPO3 11.5 - remove support for TYPO3 9.5
*	[TASK] Add documentations configuration



## 2022-05-12  Release of version 2.2.2

*	[BUGFIX] Fix explode ViewHelper limit



## 2022-05-12  Release of version 2.2.1

*	[BUGFIX] Fix spaces in beginning of a line after a line break
*	[TASK] Add replace and explode ViewHelper



## 2021-06-10  Release of version 2.2.0

*	[FEATURE] Add barcode ViewHelper



2021-01-13  ## Release of version 2.1.5

*	[BUGFIX] Fix deprecation notices in old PHP array usage



## 2020-11-30  Release of version 2.1.4

*	[TASK] Remove quotation mark from description



## 2020-11-19  Release of version 2.1.3

*	[TASK] Extend extra tags in composer.json



## 2020-06-19  Release of version 2.1.2

*	[TASK] Add extra tags in composer.json
*	[TASK] Reformat ChangeLog.md



## 2020-06-19  Release of version 2.1.1

*	[BUGFIX] Remove use statements from ext_tables and ext_localconf



## 2020-04-06  Release of version 2.1.0

*	[BUGFIX] Fix Flexform registration and render types
*	[TASK] Source code optimizations
*	[BUGFIX] Fix usage of $EXTKEY for TYPO3 10.4
*	[TASK] Migration for TYPO3 10.4
*	[TASK] FPDF library upgrade



## 2020-04-02  Release of version 2.0.5

*	[BUGFIX] Fix output to local file



## 2020-03-02  Release of version 2.0.4

*	[BUGFIX] Fix GetStringWidth ViewHelper parameter definition



## 2019-10-13  Release of version 2.0.3

*	[TASK] Add Gitlab-CI configuration.
*	[TASK] Change image urls in the documentation.



## 2019-02-27  Release of version 2.0.2

*	[BUGFIX] Removing DEV identifier.



## 2019-01-28  Release of version 2.0.1

*	[BUGFIX] Fixing argument definition in SetFontSize ViewHelper.



## 2019-01-28  Release of version 2.0.0

*	[TASK] Migration for TYPO3 9.5.
*	[FEATURE] Adding paragraphMarginBottom attribute in HtmlMultiCell-ViewHelper.
*	[BUGFIX] Removing double UTF8 encoding in Cell-ViewHelper.
*	[TASK] Optimizing spaces between ol/ul li in HtmlMultiCell.



## 2018-08-23  Release of version 1.13.0

*	[FEATURE] Adding HtmlMultiCell ViewHelper, which is able to handle the following HTML tags: b, u, i, a, p, br, strike, strong, small, em, ul, ol and li
*	[BUGFIX] Fixing conposer.json.
*	[FEATURE] Adding UTF8 Encode ViewHelper.
*	[BUGFIX] Fixing encoding issue in Cell ViewHelper.



## 2018-06-04  Release of version 1.12.0

*	[FEATURE] Adding possibility to pass variables into Footer-Template (see Readme.md).



## 2018-02-22  Release of version 1.11.1

*	[BUGFIX] Fixing image path issue by using config.absRefPrefix = /.



## 2018-02-02  Release of version 1.11.0

*	[BUGFIX] Fixing image path issue by using config.absRefPrefix = /.
*	[FEATURE] Adding IsValidImage-ViewHelper for checking a valid image type. It check if an image is vector based and if the image owns a supported file extensions.
*	[BUGFIX] Fixing UseTemplate-ViewHelper, which import the first page twice.
*	[TASK] Updating the FPDI library to version 2.0.0.



## 2018-01-13  Release of version 1.10.1

*	[BUGFIX] Fixing AddFont-ViewHelper.



## 2018-01-11  Release of version 1.10.0

*	[FEATURE] Adding support for EPS and AI image files.
*	[FEATURE] Adding custom sizes for pages. Instead of using A4, just use 213x321 in mm.



## 2017-11-23  Release of version 1.9.0

*	[TASK] Get template container variable optimization
*	[TASK] Adding new demo in documentation
*	[FEATURE] Adding ViewHelper for GetImageHeightFromWidth and GetImageWidthFromHeight



## 2017-11-19  Release of version 1.8.0

*	[FEATURE] Adding UTF8-Decode ViewHelper



## 2017-10-24  Release of version 1.7.0

*	[BUGFIX] Fixing image ViewHelper. By using config.absRefPrefix the image path is broken.
*	[FEATURE] Adding Footer feature
*	[BUGFIX] Fixing ViewHelper attribute inline line breaks



## 2017-09-27  Release of version 1.6.0

*	[FEATURE] Adding GetImageOrientation ViewHelper for identifying, if an image is portrait, landscape or squared.
*	[FEATURE] Adding SetFontSpacing ViewHelper for setting letter spacing.
*	[BUGFIX] Fixing pageno ViewHelper
*	[FEATURE] Adding GetIndexOrientation ViewHelper for writing a customized index.



## 2017-09-25  Release of version 1.5.0

*	[BUGFIX] Fixing page module content preview
*	[FEATURE] Adding Bookmarks feature (see: http://fpdf.org/en/script/script1.php).
*	[FEATURE] Adding Index feature (see: http://fpdf.org/en/script/script13.php).
*	[FEATURE] Adding GoToPage-ViewHelper for getting to previous pages (see: FpdfTutorialIntroIndex.html example).
*	[BUGFIX] Fixing encoding of windows quotes
*	[BUGFIX] Fixing style (N === '') in SetFont PDF-ViewHelper



## 2017-07-09  Release of version 1.4.2

*	[BUGFIX] Support for destination S in PDF-ViewHelper
*	[BUGFIX] Fixing exit in PDF-ViewHelper



## 2017-06-15  Release of version 1.4.1

*	[TASK] Fixing version numbers



## 2017-06-14  Release of version 1.4.0

*	[TASK] Migration to TYPO3 8.6



## 2017-04-11  Release of version 1.3.0

*	[FEATURE] Adding Debug/FontDump-ViewHelper for dumping all characters with ASCII-No.
*	[FEATURE] Adding Debug/StringCharacterDump-ViewHelper for dumping all characters from a string (with ASCII-No.)
*	[FEATURE] PDF-ViewHelper now got a *characterMap* attributes, which allows you to map characters to other ASCII-No. within the PDF-Font.
*	[TASK] SetFont-ViewHelper: Attribute style: Default value is now 'N'
*	[TASK] UseTemplate-ViewHelper: Attribute pageNo: Default value is now '0'
*	[TASK] Documentation
*	[BUGFIX] Fixing GetStringWidth ViewHelper



## 2015-09-02  Release of version 1.2.1

*	[BUGFIX] Fixing conversion of the euro symbol



## 2015-09-02  Release of version 1.2.0

*	[TASK] PHP 7 compatibility
*	[FEATURE] Adding GetPageWidth- and GetPageHeight-ViewHelper
*	[TASK] Upgrade to FPDF 1.8.1
*	[FEATURE] Adding hex color parameter for SetDrawColor-, SetFillColor- and SetTextColor-ViewHelper



## 2015-05-19  Release of version 1.1.0

*	[FEATURE] Adding FPDI feature, merge existing PDF-Pages to PDF
