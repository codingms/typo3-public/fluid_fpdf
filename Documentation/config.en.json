{
    "title": "TYPO3 Fluid-FPDF",
    "subtitle": "This extension provides you a complete set of ViewHelpers for dealing with FPDF by using Fluid. You can use the ViewHelpers easily in own Extensions just by defining the fpdf-Namespace. Additionally you're able to use the build in plugin, for displaying predefined PDF. This plugin offers you automatically all available PDFs.",
    "logo": "../Resources/Public/Icons/Extension.svg",
    "markdown": "../Readme.en.md",
    "meta": {
        "description": "TYPO3 Fluid-FPDF - This extension provides you a complete set of ViewHelpers for dealing with FPDF by using Fluid. You can use the ViewHelpers easily in own Extensions just by defining the fpdf-Namespace. Additionally you're able to use the build in plugin, for displaying predefined PDF. This plugin offers you automatically all available PDFs.",
        "abstract": "TYPO3 - Fluid-FPDF",
        "keywords": "TYPO3, Extension, PDF, Fluid, FPDF",
        "og:title": "TYPO3 Fluid-FPDF",
        "og:description": "TYPO3 Fluid-FPDF - This extension provides you a complete set of ViewHelpers for dealing with FPDF by using Fluid. You can use the ViewHelpers easily in own Extensions just by defining the fpdf-Namespace. Additionally you're able to use the build in plugin, for displaying predefined PDF. This plugin offers you automatically all available PDFs."
    },
    "features": [
        "All FPDF library functions available as ViewHelpers",
        "PDFS can be saved as a file or output directly as download",
        "Customised PDF design can be set as background image",
        "PDF content listings/indexes/table-of-content can be created",
        "Font subsetting available (text from RTE is parsed written in PDF, possible tags: h1, h2, h3, h4, h5, b, u, i, a, p, br, pagebreak, strike, strong, small, em, ul, ol, li, dl, dt, dd)",
        "Bookmarks incl. linking can be generated",
        "Barcode generator",
        "Rotating text possible",
        "Free-of-charge with an open source license"
    ],
    "links": {
        "product": {
            "link": "https://www.coding.ms/typo3-extensions/typo3-fluid-fpdf/",
            "name": "TYPO3 Fluid-FPDF Productdetails"
        },
        "documentation": {
          "link": "https://www.coding.ms/documentation/typo3-fluid-fpdf",
          "name": "TYPO3 Fluid-FPDF Documentation"
        },
        "fpdf-org": {
            "link": "http://www.fpdf.org/",
            "name": "FPDF.org"
        },
        "fpdf.de": {
            "link": "http://www.fpdf.de/",
            "name": "FPDF.de"
        },
        "download": {
            "link": "https://typo3.org/extensions/repository/view/fluid_fpdf",
            "name": "TYPO3 Fluid-FPDF Extension Download"
        }
    },
    "pages": [
        {
            "title": "Configuration",
            "pages": [
                {
                    "title": "TypoScript constants settings",
                    "markdown": "Configuration/Constants.en.md",
                    "meta": {
                        "description": "TypoScript constants for configuring TYPO3 Fluid FPDF",
                        "abstract": "How you can configure TYPO3 Fluid FPDF with TypoScript constants",
                        "keywords": "TYPO3, extension, TYPO3 Fluid FPDF, Configuration, TypoScript, Constants",
                        "og:title": "TYPO3 Fluid FPDF - TypoScript constants",
                        "og:description": "TypoScript constants for configuring TYPO3 Fluid FPDF"
                    }
                }
            ]
        },
        {
            "title": "Demos",
            "pages": [
                {
                    "title": "FPDF Tutorial 1",
                    "markdown": "Demos/FirstTutorial.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial 1 (code sample)",
                        "abstract": "Tutorial 1 (code sample)",
                        "keywords": "TYPO3, extension, FPDF, PDF, tutorial, code sample",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial 1",
                        "og:description": "Tutorial 1 (code sample)"
                    }
                },
                {
                    "title": "FPDF Tutorial Bookmarks",
                    "markdown": "Demos/Bookmarks.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Code sample for the bookmarks",
                        "abstract": "Code sample for the bookmarks",
                        "keywords": "TYPO3, extension, FPDF, PDF, tutorial, Code sample, bookmarks",
                        "og:title": "TYPO3 Fluid-FPDF - Code sample for the bookmarks",
                        "og:description": "Tutorial: Code sample for the bookmarks"
                    }
                },
                {
                    "title": "FPDF Tutorial Debugging",
                    "markdown": "Demos/Debugging.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Code sample for debugging",
                        "abstract": "Code sample for debugging",
                        "keywords": "TYPO3, extension, FPDF, PDF, tutorial, Code sample, debugging",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Code sample for debugging",
                        "og:description": "Tutorial: Code sample for debugging"
                    }
                },
                {
                    "title": "FPDF Tutorial Index",
                    "markdown": "Demos/Index.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an index",
                        "abstract": "Code sample for an index",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Code sample, index",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an index",
                        "og:description": "Tutorial: Code sample for an index"
                    }
                },
                {
                    "title": "FPDF Tutorial Intro Index",
                    "markdown": "Demos/IntroIndex.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an intro index",
                        "abstract": "Codebeispiel für ein Inhaltsverzeichnis",
                        "keywords": "TYPO3, extension, FPDF, PDF, tutorial, Code sample, intro index",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an intro index",
                        "og:description": "Tutorial: Code sample for an intro index"
                    }
                },
                {
                    "title": "FPDF Tutorial Custom Index",
                    "markdown": "Demos/CustomIndex.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an custom index",
                        "abstract": "Code sample for an custom index",
                        "keywords": "TYPO3, extension, FPDF, PDF, tutorial, code sample, custom, index",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an custom index",
                        "og:description": "Tutorial: Code sample for an custom index"
                    }
                },
                {
                    "title": "FPDF Tutorial Footer",
                    "markdown": "Demos/Footer.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an footer",
                        "abstract": "Code sample for an footer",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, code sample, footer",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an footer",
                        "og:description": "Tutorial: Code sample for an footer"
                    }
                },
                {
                    "title": "FPDF / FPDI",
                    "markdown": "Demos/Fpdi.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Code sample for FPDF/FPDI",
                        "abstract": "Code sample for FPDF/FPDI",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Code sample, FPDI",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Code sample for FPDF/FPDI",
                        "og:description": "Tutorial: Code sample for FPDF/FPDI"
                    }
                },
                {
                    "title": "FPDF Tutorial Barcode",
                    "markdown": "Demos/Barcode.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an barcode",
                        "abstract": "Code sample for an barcode",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, code sample, barcode",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Code sample for an barcode",
                        "og:description": "Tutorial: Code sample for an barcode"
                    }
                },
                {
                    "title": "FPDF Tutorial HTML MultiCell for RTE-Texte",
                    "markdown": "Demos/HtmlMultiCell.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Code sample for parsing HTML from a rich-text-editor/RTE",
                        "abstract": "Code sample for parsing HTML from a rich-text-editor/RTE",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, code sample, RTE, HTML, Rich-Text",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Code sample for parsing HTML from a rich-text-editor/RTE",
                        "og:description": "Tutorial: Code sample for parsing HTML from a rich-text-editor/RTE"
                    }
                }
            ]
        },
        {
            "title": "How To",
            "pages": [
                {
                    "title": "Add a missing font character",
                    "markdown": "HowTo/AddAMissingFontCharacter.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - How to add a missing font character.",
                        "abstract": "How to add a missing font character.",
                        "keywords": "TYPO3, extension, FPDF, PDF, font, character",
                        "og:title": "TYPO3 Fluid-FPDF - Add a missing font character",
                        "og:description": "How to add a missing font character."
                    }
                },
                {
                    "title": "Creating PDF as s string",
                    "markdown": "HowTo/CreatingPdfAsString.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - How to create a PDF as a string.",
                        "abstract": "Create PDF as string",
                        "keywords": "TYPO3, extension, FPDF, PDF, String",
                        "og:title": "TYPO3 Fluid-FPDF - Creating PDF as s string",
                        "og:description": "How to create a PDF as a string."
                    }
                },
                {
                    "title": "Dump all font characters",
                    "markdown": "HowTo/DumpAllFontCharacters.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - How to print all characters when a character is not displayed correctly.",
                        "abstract": "Dump all font characters",
                        "keywords": "TYPO3, extension, FPDF, PDF, font characters",
                        "og:title": "TYPO3 Fluid-FPDF - Dump all font characters",
                        "og:description": "How to print all characters when a character is not displayed correctly."
                    }
                },
                {
                    "title": "How to set background PDFs",
                    "markdown": "HowTo/HowToSetBackgroundPdf.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - How to set background PDFs.",
                        "abstract": "Set background PDFs",
                        "keywords": "TYPO3, extension, FPDF, PDF, background, pdf",
                        "og:title": "TYPO3 Fluid-FPDF - How to set background PDFs",
                        "og:description": "How to set background PDFs."
                    }
                },
                {
                    "title": "How to use own fonts",
                    "markdown": "HowTo/HowToUseOwnFonts.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - How to use custom fonts by converting them to a special format.",
                        "abstract": "Use custom fonts",
                        "keywords": "TYPO3, extension, FPDF, PDF, custom fonts",
                        "og:title": "TYPO3 Fluid-FPDF - So verwendest Du eigene Schriftarten",
                        "og:description": "How to use custom fonts by converting them to a special format."
                    }
                },
                {
                    "title": "Using variables in Footer",
                    "markdown": "HowTo/UsingVariablesInFooter.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - How to use variables per footerArguments attribute in footer.",
                        "abstract": "Using variables in the footer",
                        "keywords": "TYPO3, extension, FPDF, PDF, Variablen, footer",
                        "og:title": "TYPO3 Fluid-FPDF - Using variables in the footer",
                        "og:description": "How to use variables per footerArguments attribute in footer."
                    }
                },
                {
                    "title": "Trigger new page in HTML output",
                    "markdown": "HowTo/TriggerAddPageInHtmlOutput.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - How to trigger a new page in HTML output.",
                        "abstract": "Trigger new page in HTML output",
                        "keywords": "TYPO3, extension, FPDF, PDF, trigger, page",
                        "og:title": "TYPO3 Fluid-FPDF - Trigger new page in HTML output",
                        "og:description": "How to trigger a new page in HTML output."
                    }
                }
            ]
        },
        {
            "title": "Usage",
            "pages": [
                {
                    "title": "View-Helper",
                    "markdown": "Usage/ViewHelper.en.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Listing and description of view helpers that you can use in this extension.",
                        "abstract": "Listing and description of view helper",
                        "keywords": "TYPO3, extension, FPDF, PDF, ViewHelper, listing",
                        "og:title": "TYPO3 Fluid-FPDF - View-Helper",
                        "og:description": "Listing and description of view helpers that you can use in this extension."
                    }
                }
            ]
        }
    ]
}
