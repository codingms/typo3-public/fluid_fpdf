# How to trigger a new page in long HTML output?

If you like to trigger a new page within a longer HTML output, just insert a `<p>ADD_PAGE</p> in the HTML.
