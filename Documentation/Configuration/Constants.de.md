# TypoScript-Konstanten Einstellungen


## Allgemein

| **Konstante**    | plugin.tx_fluidfpdf.view.templateRootPath   |
|:-----------------|:--------------------------------------------|
| **Label**        | Pfad zu Template (FE)                       |
| **Beschreibung** |                                             |
| **Typ**          | string                                      |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Templates/ |

| **Konstante**    | plugin.tx_fluidfpdf.view.partialRootPath    |
|:-----------------|:--------------------------------------------|
| **Label**        | Pfad zu Template-Partials (FE)              |
| **Beschreibung** |                                             |
| **Typ**          | string                                      |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Partials/  |

| **Konstante**    | plugin.tx_fluidfpdf.persistence.storagePid  |
|:-----------------|:--------------------------------------------|
| **Label**        | Standard Datensatz Container                |
| **Beschreibung** |                                             |
| **Typ**          | string                                      |
| **Standardwert** |                                             |



