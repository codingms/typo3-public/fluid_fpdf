# TypoScript constant settings


## General

| **Constant**      | plugin.tx_fluidfpdf.view.templateRootPath   |
| :---------------- | :------------------------------------------ |
| **Label**         | Path to template templates (FE)             |
| **Description**   |                                             |
| **Type**          | string                                      |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Templates/ |

| **Constant**      | plugin.tx_fluidfpdf.view.partialRootPath    |
| :---------------- | :------------------------------------------ |
| **Label**         | Path to template partials (FE)              |
| **Description**   |                                             |
| **Type**          | string                                      |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Partials/  |

| **Constant**      | plugin.tx_fluidfpdf.persistence.storagePid  |
| :---------------- | :------------------------------------------ |
| **Label**         | Default storage PID                         |
| **Description**   |                                             |
| **Type**          | string                                      |
| **Default value** |                                             |



