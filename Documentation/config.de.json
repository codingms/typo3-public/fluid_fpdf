{
    "title": "TYPO3 Fluid-FPDF",
    "subtitle": "Diese Erweiterung bietet Dir einen vollständigen Satz von ViewHelpern für den Umgang mit FPDF mithilfe von Fluid. Du kannst die ViewHelper einfach in eigenen Erweiterungen verwenden, indem Du einfach den fpdf-Namespace definierst. Zusätzlich kannst du das eingebaute Plugin verwenden, um vordefinierte PDF anzuzeigen. Dieses Plugin bietet Dir automatisch alle verfügbaren PDFs.",
    "logo": "../Resources/Public/Icons/Extension.svg",
    "markdown": "../Readme.de.md",
    "meta": {
        "description": "TYPO3 Fluid-FPDF - Diese Erweiterung bietet Dir einen vollständigen Satz von ViewHelpern für den Umgang mit FPDF mithilfe von Fluid. Du kannst die ViewHelper einfach in eigenen Erweiterungen verwenden, indem Du einfach den fpdf-Namespace definierst. Zusätzlich kannst Du das eingebaute Plugin verwenden, um vordefinierte PDF anzuzeigen. Dieses Plugin bietet Dir automatisch alle verfügbaren PDFs.",
        "abstract": "TYPO3 - Fluid-FPDF",
        "keywords": "TYPO3, Extension, PDF, Fluid, FPDF",
        "og:title": "TYPO3 Fluid-FPDF",
        "og:description": "TYPO3 Fluid-FPDF - Diese Erweiterung bietet Dir einen vollständigen Satz von ViewHelpern für den Umgang mit FPDF mithilfe von Fluid. Du kannst die ViewHelper einfach in eigenen Erweiterungen verwenden, indem Du einfach den fpdf-Namespace definierst. Zusätzlich kannst Du das eingebaute Plugin verwenden, um vordefinierte PDF anzuzeigen. Dieses Plugin bietet Dir automatisch alle verfügbaren PDFs."
    },
    "features": [
        "Alle FPDF Bibliotheksfunktionen als ViewHelper",
        "PDFs können als Datei gespeichert oder direkt als Download ausgegeben werden",
        "Individuelles PDF-Design ist als Hintergrund hinterlegbar",
        "PDF-Inhaltsverzeichnisse lassen sich erstellen",
        "Font-Subsetting ist möglich (Text aus RTE wird geparst und ins PDF geschrieben, mögliche Tags: h1, h2, h3, h4, h5, b, u, i, a, p, br, pagebreak, strike, strong, small, em, ul, ol, li, dl, dt, dd)",
        "Bookmarks inkl. Verlinkung können generiert werden",
        "Barcode-Generator",
        "Text-Rotation möglich",
        "Kostenfrei durch Open-Source-Lizenz"
    ],
    "links": {
        "product": {
            "link": "https://www.coding.ms/de/typo3-extensions/typo3-fluid-fpdf/",
            "name": "TYPO3 Fluid-FPDF Produktdetails"
        },
        "documentation": {
          "link": "https://www.coding.ms/de/dokumentation/typo3-fluid-fpdf",
          "name": "TYPO3 Fluid-FPDF Dokumentation"
        },
        "fpdf-org": {
            "link": "http://www.fpdf.org/",
            "name": "FPDF.org"
        },
        "fpdf.de": {
            "link": "http://www.fpdf.de/",
            "name": "FPDF.de"
        },
        "download": {
            "link": "https://typo3.org/extensions/repository/view/fluid_fpdf",
            "name": "TYPO3 Fluid-FPDF Extension-Download"
        }
    },
    "pages": [
        {
            "title": "Konfiguration",
            "originalTitle": "Configuration",
            "pages": [
                {
                    "title": "TypoScript-Konstanten Einstellungen",
                    "originalTitle": "TypoScript constants settings",
                    "markdown": "Configuration/Constants.de.md",
                    "meta": {
                        "description": "TypoScript-Konstanten zur Konfiguration der TYPO3 Fluid FPDF",
                        "abstract": "Wie du die TYPO3 Fluid FPDF mit TypoScript-Konstanten konfigurierst",
                        "keywords": "TYPO3, extension, TYPO3 Fluid FPDF, Konfiguration, TypoScript, Konstanten",
                        "og:title": "TYPO3 Fluid FPDF - TypoScript-Konstanten",
                        "og:description": "TypoScript-Konstanten zur Konfiguration der TYPO3 Fluid FPDF"
                    }
                }
            ]
        },
        {
            "title": "Demos",
            "originalTitle": "Demos",
            "pages": [
                {
                    "title": "FPDF Tutorial 1",
                    "originalTitle": "FPDF Tutorial 1",
                    "markdown": "Demos/FirstTutorial.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial 1 (Codebeispiel)",
                        "abstract": "Tutorial 1 (Codebeispiel)",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial 1",
                        "og:description": "Tutorial 1 (Codebeispiel)"
                    }
                },
                {
                    "title": "FPDF Tutorial Bookmarks",
                    "originalTitle": "FPDF Tutorial Bookmarks",
                    "markdown": "Demos/Bookmarks.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für die Bookmarks",
                        "abstract": "Codebeispiel für die Bookmarks",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel, Bookmarks",
                        "og:title": "TYPO3 Fluid-FPDF - Codebeispiel für die Bookmarks",
                        "og:description": "Tutorial: Codebeispiel für die Bookmarks"
                    }
                },
                {
                    "title": "FPDF Tutorial Debugging",
                    "originalTitle": "FPDF Tutorial Debugging",
                    "markdown": "Demos/Debugging.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für das Debugging",
                        "abstract": "Codebeispiel für das Debugging",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel, Debugging",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für das Debugging",
                        "og:description": "Tutorial: Codebeispiel für das Debugging"
                    }
                },
                {
                    "title": "FPDF Tutorial Index",
                    "originalTitle": "FPDF Tutorial Index",
                    "markdown": "Demos/Index.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für ein Index",
                        "abstract": "Codebeispiel für ein Index",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel, Index",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für ein Index",
                        "og:description": "Tutorial: Codebeispiel für ein Index"
                    }
                },
                {
                    "title": "FPDF Tutorial Intro Index",
                    "originalTitle": "FPDF Tutorial Intro Index",
                    "markdown": "Demos/IntroIndex.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für ein Inhaltsverzeichnis",
                        "abstract": "Codebeispiel für ein Inhaltsverzeichnis",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel, Inhaltsverzeichnis",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für ein Inhaltsverzeichnis",
                        "og:description": "Tutorial: Codebeispiel für ein Inhaltsverzeichnis"
                    }
                },
                {
                    "title": "FPDF Tutorial Custom Index",
                    "originalTitle": "FPDF Tutorial Custom Index",
                    "markdown": "Demos/CustomIndex.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für ein benutzerdefinierter Index",
                        "abstract": "Codebeispiel für benutzerdefinierter Index",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel, benutzerdefiniert, Inhaltsverzeichnis",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für ein benutzerdefinierter Index",
                        "og:description": "Tutorial: Codebeispiel für ein benutzerdefinierter Index"
                    }
                },
                {
                    "title": "FPDF Tutorial Footer",
                    "originalTitle": "FPDF Tutorial Footer",
                    "markdown": "Demos/Footer.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für einen Seitenfuß",
                        "abstract": "Codebeispiel für einen Seitenfuß",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel, Seitenfuß, footer",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für einen Seitenfuß",
                        "og:description": "Tutorial: Codebeispiel für einen Seitenfuß"
                    }
                },
                {
                    "title": "FPDF / FPDI",
                    "originalTitle": "FPDF / FPDI",
                    "markdown": "Demos/Fpdi.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für FPDF/FPDI",
                        "abstract": "Codebeispiel für FPDF/FPDI",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel, FPDI",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für FPDF/FPDI",
                        "og:description": "Tutorial: Codebeispiel für FPDF/FPDI"
                    }
                },
                {
                    "title": "FPDF Tutorial Barcode",
                    "originalTitle": "FPDF Tutorial Barcode",
                    "markdown": "Demos/Barcode.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für einen Barcode",
                        "abstract": "Codebeispiel für einen Barcode",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel, Barcode",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für einen Barcode",
                        "og:description": "Tutorial: Codebeispiel für einen Barcode"
                    }
                },
                {
                    "title": "FPDF Tutorial HTML MultiCell für RTE-Texte",
                    "originalTitle": "FPDF Tutorial HTML MultiCell for RTE-Texte",
                    "markdown": "Demos/HtmlMultiCell.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für das parsen von HTML aus einen Rich-Text-Editor/RTE",
                        "abstract": "Codebeispiel für das parsen von HTML aus einen Rich-Text-Editor/RTE",
                        "keywords": "TYPO3, extension, FPDF, PDF, Tutorial, Codebeispiel, RTE, HTML, Rich-Text",
                        "og:title": "TYPO3 Fluid-FPDF - Tutorial: Codebeispiel für das parsen von HTML aus einen Rich-Text-Editor/RTE",
                        "og:description": "Tutorial: Codebeispiel ffür das parsen von HTML aus einen Rich-Text-Editor/RTE"
                    }
                }
            ]
        },
        {
            "title": "How To",
            "originalTitle": "How To",
            "pages": [
                {
                    "title": "Füge ein fehlendes Schriftzeichen hinzu",
                    "originalTitle": "Add a missing font character",
                    "markdown": "HowTo/AddAMissingFontCharacter.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Wie man ein fehlendes Schriftzeichen hinzufügt.",
                        "abstract": "Wie man ein fehlendes Schriftzeichen hinzufügt.",
                        "keywords": "TYPO3, extension, FPDF, PDF, Schriftzeichen",
                        "og:title": "TYPO3 Fluid-FPDF - Füge ein fehlendes Schriftzeichen hinzu",
                        "og:description": "Wie man ein fehlendes Schriftzeichen hinzufügt."
                    }
                },
                {
                    "title": "PDF als String erstellen",
                    "v": "Creating PDF as s string",
                    "markdown": "HowTo/CreatingPdfAsString.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Wie man ein PDF als String erstellt.",
                        "abstract": "PDF als String erstellen",
                        "keywords": "TYPO3, extension, FPDF, PDF, String",
                        "og:title": "TYPO3 Fluid-FPDF - PDF als String erstellen",
                        "og:description": "Wie man ein PDF als String erstellt."
                    }
                },
                {
                    "title": "Alle Schriftzeichen ausgeben",
                    "originalTitle": "Dump all font characters",
                    "markdown": "HowTo/DumpAllFontCharacters.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Wie man alle Schriftzeichen ausgibt wenn ein Zeichen nicht richtig dargestellt wird.",
                        "abstract": "Alle Schriftzeichen ausgeben",
                        "keywords": "TYPO3, extension, FPDF, PDF, Schriftzeichen",
                        "og:title": "TYPO3 Fluid-FPDF - Alle Schriftzeichen ausgeben",
                        "og:description": "Wie man alle Schriftzeichen ausgibt wenn ein Zeichen nicht richtig dargestellt wird."
                    }
                },
                {
                    "title": "So legen Sie Hintergrund-PDFs fest",
                    "originalTitle": "How to set background PDFs",
                    "markdown": "HowTo/HowToSetBackgroundPdf.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Wie man Hintergrund-PDFs festlegt.",
                        "abstract": "Hintergrund-PDFs festlegen",
                        "keywords": "TYPO3, extension, FPDF, PDF, Hintergrund, pdf",
                        "og:title": "TYPO3 Fluid-FPDF - So legen Sie Hintergrund-PDFs fest",
                        "og:description": "Wie man Hintergrund-PDFs festlegt."
                    }
                },
                {
                    "title": "So verwendest Du eigene Schriftarten",
                    "originalTitle": "How to use own fonts",
                    "markdown": "HowTo/HowToUseOwnFonts.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Wie man eigene Schriftarten verwendet indem man sie in ein spezielles Format umwandelt.",
                        "abstract": "Eigene Schriftarten verwenden",
                        "keywords": "TYPO3, extension, FPDF, PDF, Schriftarten",
                        "og:title": "TYPO3 Fluid-FPDF - So verwendest Du eigene Schriftarten",
                        "og:description": "Wie man eigene Schriftarten verwendet indem man sie in ein spezielles Format umwandelt."
                    }
                },
                {
                    "title": "Verwenden von Variablen in der Fußzeile",
                    "originalTitle": "Using variables in Footer",
                    "markdown": "HowTo/UsingVariablesInFooter.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Wie man Variablen per footerArguments-Attribut in der Fußzeile verwendet.",
                        "abstract": "Verwenden von Variablen in der Fußzeile",
                        "keywords": "TYPO3, extension, FPDF, PDF, Variablen, Fußzeile",
                        "og:title": "TYPO3 Fluid-FPDF - Verwenden von Variablen in der Fußzeile",
                        "og:description": "Wie man Variablen per footerArguments-Attribut in der Fußzeile verwendet."
                    }
                },
                {
                    "title": "Auslösen einer neuen Seite in der HTML-Ausgabe",
                    "originalTitle": "Trigger new page in HTML output",
                    "markdown": "HowTo/TriggerAddPageInHtmlOutput.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Wie man eine neue Seite in der HTML-Ausgabe auslöst.",
                        "abstract": "Auslösen einer neuen Seite",
                        "keywords": "TYPO3, extension, FPDF, PDF, Seite, auslösen",
                        "og:title": "TYPO3 Fluid-FPDF - Auslösen einer neuen Seite in der HTML-Ausgabe",
                        "og:description": "Wie man eine neue Seite in der HTML-Ausgabe auslöst."
                    }
                }
            ]
        },
        {
            "title": "Verwendung",
            "originalTitle": "Usage",
            "pages": [
                {
                    "title": "View-Helper",
                    "originalTitle": "View-Helper",
                    "markdown": "Usage/ViewHelper.de.md",
                    "meta": {
                        "description": "TYPO3 Fluid-FPDF - Auflistung und Beschreibung von View-Helper, die Du in dieser Extension einsetzen kannst.",
                        "abstract": "Auflistung und Beschreibung von View-Helper",
                        "keywords": "TYPO3, extension, FPDF, PDF, ViewHelper, Auflistung",
                        "og:title": "TYPO3 Fluid-FPDF - View-Helper",
                        "og:description": "Auflistung und Beschreibung von View-Helper, die Du in dieser Extension einsetzen kannst."
                    }
                }
            ]
        }
    ]
}
