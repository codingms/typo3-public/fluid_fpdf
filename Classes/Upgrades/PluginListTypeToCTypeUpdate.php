<?php

declare(strict_types=1);

namespace CodingMs\FluidFpdf\Upgrades;

use CodingMs\AdditionalTca\Upgrades\AbstractListTypeToCTypeV12V13;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('fluidFpdfPluginListTypeToCTypeUpdate')]
final class PluginListTypeToCTypeUpdate extends AbstractListTypeToCTypeV12V13
{
    protected function getListTypeToCTypeMapping(): array
    {
        return [
            'fluidfpdf_fpdf' => 'fluidfpdf_fpdf'
        ];
    }

    public function getTitle(): string
    {
        return 'PluginListTypeToCTypeUpdate Upgrade Wizard';
    }

    public function getDescription(): string
    {
        return 'EXT:fluid_fpdf: Switches ListTypes to CTypes for TYPO3 V12 and V13';
    }
}
