<?php

declare(strict_types=1);

namespace CodingMs\FluidFpdf\EventListener;

use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

final class DrawItem
{
    public function __invoke(PageContentPreviewRenderingEvent $event): void
    {
        if ($event->getTable() !== 'tt_content') {
            return;
        }

        if (isset($event->getRecord()['CType'])
            && $event->getRecord()['CType'] === 'list'
            && $event->getRecord()['list_type'] === 'fluidfpdf_pdf') {
            $languageService =GeneralUtility::makeInstance(LanguageServiceFactory::class) ->createFromUserPreferences($GLOBALS['BE_USER']);
            $lllFile = 'LLL:EXT:fluid_fpdf/Resources/Private/Language/locallang_db.xlf:';
            // Get general tab data
            if (!empty($event->getRecord()['pi_flexform'])) {
                $flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
                $flexformArray = $flexFormService->convertFlexFormContentToArray($event->getRecord()['pi_flexform']);
                $itemContent = '';
                $itemContent .= '<table class="table table-bordered table-striped">';
                if (trim($event->getRecord()['header']) !== '') {
                    $itemContent .= $this->getTableRow(
                        'Title',
                        $event->getRecord()['header']
                    );
                }
                if (trim($flexformArray['settings']['template']) !== '') {
                    $itemContent .= $this->getTableRow(
                        $languageService->sL($lllFile . 'tx_fluidfpdf_flexform_pdf.template'),
                        $flexformArray['settings']['template']
                    );
                }
                $itemContent .= '</table>';

                $event->setPreviewContent($itemContent);
            }
        }
    }

    /**
     * @param string $label
     * @param mixed $value
     * @return string
     */
    protected function getTableRow(string $label, mixed $value): string
    {
        $itemContent = '';
        $itemContent .= '<tr>';
        $itemContent .= '<th>' . $label . '</th>';
        if (is_array($value)) {
            $itemContent .= '<td>' . implode(', ', $value) . '</td>';
        } else {
            $itemContent .= '<td>' . $value . '</td>';
        }
        $itemContent .= '</tr>';
        return $itemContent;
    }
}
