<?php

namespace CodingMs\FluidFpdf\Service;

use FluidFpdf;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Services for printing PDF footer
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class FooterService
{
    /**
     * Creates a footer
     *
     * @param FluidFpdf $fpdf
     */
    public function write(FluidFpdf $fpdf): void
    {
        $pdfView = new StandaloneView();
        $templateRootPath = GeneralUtility::getFileAbsFileName($fpdf->GetFooterTemplate());
        if (file_exists($templateRootPath)) {
            $pdfView->setTemplatePathAndFilename($templateRootPath);
            $pdfView->assign('fpdf', $fpdf);
            $pdfView->assign('settings', $fpdf->getFooterArguments());
            $pdfView->render();
        }
    }
}
