<?php

namespace CodingMs\FluidFpdf\ViewHelpers\Format;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Replaces $substring in $content with $replacement.
 */
class ReplaceViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * Initialize
     */
    public function initializeArguments()
    {
        $this->registerArgument('substring', 'string', 'String that should be found');
        $this->registerArgument('content', 'string', 'Content wherein the substring should be replaced');
        $this->registerArgument('replacement', 'string', 'Substring replacement');
        $this->registerArgument('count', 'int', 'Amount of replacements');
    }

    /**
     * Trims content by stripping off $characters
     *
     * @param array<string, mixed> $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return mixed
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        if (null === $arguments['content']) {
            $arguments['content'] = $renderChildrenClosure();
        }
        return str_replace(
            $arguments['substring'],
            $arguments['replacement'],
            $arguments['content'],
            $arguments['count']
        );
    }
}
