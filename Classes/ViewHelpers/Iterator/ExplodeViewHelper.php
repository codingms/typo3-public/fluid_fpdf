<?php

namespace CodingMs\FluidFpdf\ViewHelpers\Iterator;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Explodes a string.
 */
class ExplodeViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * Initialize
     */
    public function initializeArguments()
    {
        $this->registerArgument('separator', 'string', 'Separator string');
        $this->registerArgument('content', 'string', 'Content string');
        $this->registerArgument('removeEmptyValues', 'bool', 'Remove empty values', false, false);
        $this->registerArgument('limit', 'int', 'Amount of replacements', false, 0);
    }

    /**
     * Trims content by stripping off $characters
     *
     * @param array<string, mixed> $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return mixed
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        if (null === $arguments['content']) {
            $arguments['content'] = $renderChildrenClosure();
        }
        return GeneralUtility::trimExplode(
            $arguments['separator'],
            $arguments['content'],
            $arguments['removeEmptyValues'],
            $arguments['limit']
        );
    }
}
