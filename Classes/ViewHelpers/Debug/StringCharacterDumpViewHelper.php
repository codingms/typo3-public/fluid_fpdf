<?php

namespace CodingMs\FluidFpdf\ViewHelpers\Debug;

use FluidFpdf;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Writes a character dump of a string
 */
class StringCharacterDumpViewHelper extends AbstractViewHelper
{
    /**
     * Initialize
     */
    public function initializeArguments()
    {
        $this->registerArgument('string', 'string', 'String which should be dumped', false, '');
    }

    /**
     * Writes a character dump of a string
     */
    public function render(): void
    {
        $string = $this->arguments['string'];
        $debug = false;
        if (iconv('UTF-8', 'cp1252//TRANSLIT', $string) != $string) {
            $string = iconv('UTF-8', 'cp1252//TRANSLIT', $string);
        } else {
            $string = mb_convert_encoding($string, 'ISO-8859-1', 'UTF-8');
        }
        /** @phpstan-ignore-next-line */
        if ($debug) {
            echo '<pre>';
        }
        /** @var FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        for ($i = 0; $i <= strlen((string)$string); $i++) {
            /** @phpstan-ignore-next-line */
            if ($debug) {
                /** @phpstan-ignore-next-line */
                echo $string[$i] . ':' . ord($string[$i]) . '<br />';
            }
            /** @phpstan-ignore-next-line */
            $fpdf->Cell(12, 5.5, $string[$i] . ' : ');
            /** @phpstan-ignore-next-line */
            $fpdf->Cell(0, 5.5, ord($string[$i]), 0, 1);
        }
        /** @phpstan-ignore-next-line */
        if ($debug) {
            exit;
        }
    }
}
