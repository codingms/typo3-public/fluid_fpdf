<?php

namespace CodingMs\FluidFpdf\ViewHelpers\Condition;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class IsValidImageViewHelper extends AbstractConditionViewHelper
{
    /**
     * Initialize
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('publicUrl', 'string', 'Image url', true);
        $this->registerArgument('vector', 'boolean', 'Is vector image format', false, false);
    }

    /**
     * This method decides if the condition is TRUE or FALSE. It can be overridden in extending ViewHelpers to adjust functionality.
     *
     * @param array<string, mixed> $arguments ViewHelper arguments to evaluate the condition for this ViewHelper, allows for flexibility in overriding this method.
     * @return bool
     */
    protected static function evaluateCondition($arguments = null)
    {
        /** @phpstan-ignore-next-line */
        $pathInfo = pathinfo(strtolower($arguments['publicUrl']));
        $validExtensions = ['jpg', 'jpeg', 'png', 'gif'];
        /** @phpstan-ignore-next-line */
        if (true === $arguments['vector']) {
            $validExtensions = ['eps', 'ai'];
        }
        /** @phpstan-ignore-next-line */
        return in_array($pathInfo['extension'], $validExtensions);
    }
}
