<?php

declare(strict_types=1);

namespace CodingMs\FluidFpdf\Event;

use FluidFpdf;

final class DrawPdfFooterEvent
{
    protected FluidFpdf $fpdf;

    /**
     * @param FluidFpdf $fpdf
     */
    public function __construct(FluidFpdf $fpdf)
    {
        $this->fpdf = $fpdf;
    }

    /**
     * @return FluidFpdf
     */
    public function getFpdf(): FluidFpdf
    {
        return $this->fpdf;
    }
}
