# Fluid-Fpdf Extension for TYPO3

This extension provides you a complete set of ViewHelpers for dealing with FPDF by using Fluid. You can use the ViewHelpers easily in own Extensions just by defining the fpdf-Namespace. Additionally you're able to use the build in plugin, for displaying predefined PDF. This plugin offers you automatically all available PDFs.


**Features:**

*   All FPDF library functions available as ViewHelpers
*   PDFS can be saved as a file or output directly as download
*   Customised PDF design can be set as background image
*   PDF content listings/indexes/table-of-content can be created
*   Font subsetting available (text from RTE is parsed written in PDF, possible tags: h1, h2, h3, h4, h5, b, u, i, a, p, br, pagebreak, strike, strong, small, em, ul, ol, li, dl, dt, dd)
*   Bookmarks incl. linking can be generated
*   Barcode generator
*   Rotating text possible
*   Free-of-charge with an open source license

If you need some additional or custom feature - get in contact!


**Links:**

*   [Fluid-Fpdf Documentation](https://www.coding.ms/documentation/typo3-fluid-fpdf "Fluid-Fpdf Documentation")
*   [Fluid-Fpdf Bug-Tracker](https://gitlab.com/codingms/typo3-public/fluid_fpdf/-/issues "Fluid-Fpdf Bug-Tracker")
*   [Fluid-Fpdf Repository](https://gitlab.com/codingms/typo3-public/fluid_fpdf "Fluid-Fpdf Repository")
*   [TYPO3 Fluid-FPDF Productdetails](https://www.coding.ms/typo3-extensions/typo3-fluid-fpdf/ "TYPO3 Fluid-FPDF Productdetails")
*   [TYPO3 Fluid-FPDF Documentation](https://www.coding.ms/documentation/typo3-fluid-fpdf "TYPO3 Fluid-FPDF Documentation")
*   [FPDF.org](http://www.fpdf.org/ "FPDF.org")
*   [FPDF.de](http://www.fpdf.de/ "FPDF.de")
*   [TYPO3 Fluid-FPDF Extension Download](https://typo3.org/extensions/repository/view/fluid_fpdf "TYPO3 Fluid-FPDF Extension Download")
