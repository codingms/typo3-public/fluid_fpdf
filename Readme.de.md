# Fluid-Fpdf Erweiterung für TYPO3

Diese Erweiterung bietet Dir einen vollständigen Satz von ViewHelpern für den Umgang mit FPDF mithilfe von Fluid. Du kannst die ViewHelper einfach in eigenen Erweiterungen verwenden, indem Du einfach den fpdf-Namespace definierst. Zusätzlich kannst du das eingebaute Plugin verwenden, um vordefinierte PDF anzuzeigen. Dieses Plugin bietet Dir automatisch alle verfügbaren PDFs.


**Features:**

*   Alle FPDF Bibliotheksfunktionen als ViewHelper
*   PDFs können als Datei gespeichert oder direkt als Download ausgegeben werden
*   Individuelles PDF-Design ist als Hintergrund hinterlegbar
*   PDF-Inhaltsverzeichnisse lassen sich erstellen
*   Font-Subsetting ist möglich (Text aus RTE wird geparst und ins PDF geschrieben, mögliche Tags: h1, h2, h3, h4, h5, b, u, i, a, p, br, pagebreak, strike, strong, small, em, ul, ol, li, dl, dt, dd)
*   Bookmarks inkl. Verlinkung können generiert werden
*   Barcode-Generator
*   Text-Rotation möglich
*   Kostenfrei durch Open-Source-Lizenz

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Fluid-Fpdf Dokumentation](https://www.coding.ms/documentation/typo3-fluid-fpdf "Fluid-Fpdf Dokumentation")
*   [Fluid-Fpdf Bug-Tracker](https://gitlab.com/codingms/typo3-public/fluid_fpdf/-/issues "Fluid-Fpdf Bug-Tracker")
*   [Fluid-Fpdf Repository](https://gitlab.com/codingms/typo3-public/fluid_fpdf "Fluid-Fpdf Repository")
*   [TYPO3 Fluid-FPDF Produktdetails](https://www.coding.ms/de/typo3-extensions/typo3-fluid-fpdf/ "TYPO3 Fluid-FPDF Produktdetails")
*   [TYPO3 Fluid-FPDF Dokumentation](https://www.coding.ms/de/dokumentation/typo3-fluid-fpdf "TYPO3 Fluid-FPDF Dokumentation")
*   [FPDF.org](http://www.fpdf.org/ "FPDF.org")
*   [FPDF.de](http://www.fpdf.de/ "FPDF.de")
*   [TYPO3 Fluid-FPDF Extension-Download](https://typo3.org/extensions/repository/view/fluid_fpdf "TYPO3 Fluid-FPDF Extension-Download")
